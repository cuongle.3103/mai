package auto.pilot.auto2.acceptancetests;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/scenario/PostOnHomePage.feature", glue = "auto.pilot.auto2")
public class AcceptanceTestSuite {}

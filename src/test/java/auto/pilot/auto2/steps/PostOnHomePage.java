package auto.pilot.auto2.steps;

import auto.pilot.auto2.pages.CreateNewPostAction;
import auto.pilot.auto2.pages.PostListOnHomeAction;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;

import java.awt.*;

public class PostOnHomePage extends UIInteractionSteps {
    @Steps
    CreateNewPostAction createNewPostAction;
    PostListOnHomeAction postListOnHomePageAction;


    @Given("the user create new post {string}")
    public void the_user_create_new_post(String content) throws InterruptedException {
        createNewPostAction.create_new_post(content);
    }



    @Given("the user select the newly created post")
    public void the_user_select_the_newly_created_post() throws InterruptedException {
        // Write code here that turns the phrase above into concrete actions
        postListOnHomePageAction.select_post();
    }


}

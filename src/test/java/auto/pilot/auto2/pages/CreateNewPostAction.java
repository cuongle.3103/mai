package auto.pilot.auto2.pages;

import auto.pilot.auto2.helper.ClickHelper;
import auto.pilot.auto2.helper.UploadMedia;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.JavascriptExecutor;

import java.awt.*;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class CreateNewPostAction extends UIInteractionSteps {
    @Steps
    ClickHelper clickHelper;
    UploadMedia uploadMedia;

    @Step("Create new post")
    public void create_new_post(String content) throws InterruptedException {
        clickHelper.click_by_js($(CreateNewPostPage.CREATE_POST_BTN),getDriver());
        $(CreateNewPostPage.POST_TEXT_AREA).sendKeys(content);
        clickHelper.click_by_js($(CreateNewPostPage.SHARE_BTN),getDriver());
        String contents = $(CreateNewPostPage.POST_TEXT_AREA_POST).getText();
        assertEquals(content,contents);
    }
}

package auto.pilot.auto2.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

public class PostListOnHomePage extends PageObject {

    public static By VIEW_MORE_OPTION = By.xpath("//div[@class='post-item '][1]//i[@class='scenario-icon icon-more-option']");
    public static By EDIT_POST_OPTION = By.xpath("//a[contains(text(),'Chỉnh sửa bài viết')]");
    public static By GO_TO_GROUP = By.xpath("//span[contains(text(),'Nhóm')]");
    public static By MY_GROUP = By.xpath("//span[contains(text(),'Test')]");
    public static By GO_TO_PAGE = By.xpath("//a[contains(text(),'Trang của bạn')]");
    public static By SELECT_MY_PAGE = By.xpath("//span[contains(text(),'Page của QM')]");
    public static By SHARE_POST_OPTION = By.cssSelector("#post-share");
    public static By CONFIRM_YES_POST = By.xpath("//button[contains(text(),'Có')]");
    public static By DELETE_POST_OPTION = By.xpath("//a[contains(text(),'Xóa bài viết')]");
    public static By SHARE_POST_BTN =By.xpath("//button[contains(text(),'Chia sẻ')]");
    public static By SUGGEST_FIND_FR = By.xpath("//svg[@class='MuiSvgIcon-root search-tips__close MuiSvgIcon-fontSizeSmall']");

}

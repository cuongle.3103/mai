package auto.pilot.auto2.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

public class
Navigation extends PageObject {

    public static By IGNORE_BTN = By.xpath("//button[text()= 'Bỏ qua']");
    public static By GIFT_BTN = By.xpath("//i[@class='scenario-icon icon-close']");
    public static By FOLLOW_FEED_BTN = By.xpath("//a[@href=\"/follow\"]");
    public static By HASHTAG_DETAIL_BTN = By.xpath("//div[@class='react-multi-carousel-list  ']");
    public static By VIDEO_DETAIL_BTN = By.xpath("//span[contains(text(),'Video')]");
}

package auto.pilot.auto2.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

public class CreateNewPostPage extends PageObject {

    public static By CREATE_POST_BTN = By.xpath( "//button[text()='Đăng bài viết']");

    public static By POST_TEXT_AREA = By.xpath ("//div[@id='textarea-create']//div[@class='public-DraftStyleDefault-block public-DraftStyleDefault-ltr']");

    public static By POST_TEXT_AREA_POST = By.xpath("//div[not(contains(@class,'share-post-container'))]/div[@class='post-item '][1]//div[@class='text-display undefined']");

    public static By POST_MEDIA = By.xpath("//div[@class='create-post__actions-item']");

    public static By POST_EMOJI = By.xpath("//div/span[@class='icon smile-icon']");

    public static By EXISTING_POST_TEXT_AREA = By.xpath("//div[@class='create-post__wrapper']//div[@class='DraftEditor-editorContainer']//div[@class='notranslate public-DraftEditor-content']");

    public static By SHARE_BTN = By.xpath ("//button[text()='Chia sẻ']");

    public static By UPDATE_BTN = By.xpath ("//button[text()='Cập nhật']");

}

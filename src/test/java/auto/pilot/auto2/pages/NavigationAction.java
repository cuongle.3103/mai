package auto.pilot.auto2.pages;

import auto.pilot.auto2.helper.ClickHelper;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class NavigationAction extends UIInteractionSteps {
    @Steps
    ClickHelper clickHelper;

    @Step ("Ignore suggest friend and select subject on home page")
    public void navigate_on_homepage() throws InterruptedException {
        $(Navigation.IGNORE_BTN).click();
        Thread.sleep(3000);
        $(Navigation.IGNORE_BTN).click();
        clickHelper.click_by_js($(Navigation.GIFT_BTN), getDriver());
        //follow_feed
//        $(Navigation.FOLLOW_FEED_BTN).click();
        //video
//        $(Navigation.VIDEO_DETAIL_BTN).click();
        //hashtag
//        $(Navigation.HASHTAG_DETAIL_BTN).click();
    }

}

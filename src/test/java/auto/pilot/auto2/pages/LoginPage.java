package auto.pilot.auto2.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

public class LoginPage extends PageObject {
    public static By PHONE_TXT = By.name("phone");
    public static By CONTINUE_BTN = By.xpath("//button[text()='Tiếp tục']");
    public static By PASS_TXT = By.xpath("//input[@type='password']");
    public static By LOGIN_BTN = By.xpath("//button[@type = 'submit']");
}

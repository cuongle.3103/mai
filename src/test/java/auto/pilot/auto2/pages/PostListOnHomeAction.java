package auto.pilot.auto2.pages;

import auto.pilot.auto2.helper.ClickHelper;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class PostListOnHomeAction extends UIInteractionSteps {
    @Steps
    ClickHelper clickHelper;
    @Step ("Select edit post on homepage")
    public void select_post() throws InterruptedException {
        clickHelper.click_by_js($(PostListOnHomePage.VIEW_MORE_OPTION),getDriver());
    }

    @Step("go to group")
    public void go_to_group(){
        $(PostListOnHomePage.GO_TO_GROUP).click();
        $(PostListOnHomePage.MY_GROUP).click();
    }

    @Step("feed group")
    public void feed_group(){
        $(PostListOnHomePage.GO_TO_GROUP).click();
        getDriver().navigate().back();
    }

    @Step("go to page")
    public void go_to_page() throws InterruptedException {
        $(PostListOnHomePage.GO_TO_PAGE).click();
        $(PostListOnHomePage.SELECT_MY_PAGE).click();
//        clickHelper.click_by_js($(PostListOnHomePage.SUGGEST_FIND_FR), getDriver());
    }
}

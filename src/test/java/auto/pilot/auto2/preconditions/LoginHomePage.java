package auto.pilot.auto2.preconditions;
import io.cucumber.java.Before;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;

public class LoginHomePage extends UIInteractionSteps {
    private static boolean beforeFeature = false;
    @Steps
    Login login;

    @Before("@PostOnHomePage")
    public void user_login_on_home_page() throws InterruptedException {
        if(!beforeFeature){
            beforeFeature = true;
            login.user_is_on_Home_Page_and_Login_successfully();
        }
    }
}

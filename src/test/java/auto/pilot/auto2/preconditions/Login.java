package auto.pilot.auto2.preconditions;

import auto.pilot.auto2.navigation.HomePageURLAction;
import auto.pilot.auto2.pages.NavigationAction;
import auto.pilot.auto2.pages.LoginAction;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;

public class Login extends UIInteractionSteps {
    @Steps
    HomePageURLAction HomePageURLAction;
    LoginAction loginPageAction;
    NavigationAction navigationAction;

    public void user_is_on_Home_Page_and_Login_successfully() throws InterruptedException {
        getDriver().manage().window().maximize();
        HomePageURLAction.open_home_page();
        loginPageAction.login("0946175901", "12345678a");
        navigationAction.navigate_on_homepage();
    }
}

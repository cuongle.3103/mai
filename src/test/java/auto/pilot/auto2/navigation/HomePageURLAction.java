package auto.pilot.auto2.navigation;

import net.thucydides.core.annotations.Step;

public class HomePageURLAction {
    HomePageURL gapoHomePageURL;

    @Step("Open home page")
    public void open_home_page(){
        gapoHomePageURL.open() ;
    }
}

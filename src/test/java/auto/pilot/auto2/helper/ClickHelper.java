package auto.pilot.auto2.helper;

import net.serenitybdd.core.steps.UIInteractionSteps;
import org.junit.rules.Timeout;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;


public class ClickHelper extends UIInteractionSteps {
    public void click_by_js(WebElement e, WebDriver driver) throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver ;
        js.executeScript("arguments[0].click();",e);
        TimeUnit.SECONDS.sleep(2);
    }
}
